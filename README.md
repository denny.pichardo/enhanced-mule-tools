**IMPORTANT NOTE**

This project's maintenance has been taken over by Aeontronix (please note maven group id has now changed, see docs).

Documentation: https://enhanced-mule-tools.aeontronix.com/

Repository URL: https://gitlab.com/aeontronix/oss/enhanced-mule-tools
