---
layout: home
---
# Overview

[Enhanced Mule](https://www.enhanced-mule.com) provides advanced automation and capabilities designed to help with development
and maintenance of Mule / Anypoint application.

It provide various capabilities that can either be accessed from the website, or using the Enhanced Mule Tools (either 
via it command line tool or via it's Maven Plugin)

# Features

- API Management Automated Provisioning
