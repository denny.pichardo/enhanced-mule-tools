---
layout: page
title: Licence
permalink: /license.html
nav_order: 4
---
Enhanced mule tools is licensed under the CPAL License: https://opensource.org/licenses/CPAL-1.0
