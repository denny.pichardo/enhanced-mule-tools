/*
 * Copyright (c) Aeontronix 2020
 */

package com.aeontronix.enhancedmule.tools.anypoint;

public interface Service {
    void setClient(AnypointClient client);
}
